package aeroporto;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;


public class ListarAeroporto {
    
    public void listar(JList listAeroporto) throws SQLException{
        
        try
        {
        listAeroporto.removeAll();
        DefaultListModel dfm = new DefaultListModel();
        Connection c = Conexao.ObterConexao();
        String SQL = "SELECT * FROM sc_aeroporto.Aeroporto";
        PreparedStatement ps = c.prepareStatement(SQL);
        Statement s = c.createStatement();
        ResultSet rs = s.executeQuery(SQL);
        while(rs.next()){
            dfm.addElement(rs.getString("nomeAeroporto"));
            listAeroporto.setModel(dfm);
            c.close();
        }
        c.close();
        }catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
    public void listar2(JList listAviao){
        
        try {
            listAviao.removeAll();
            DefaultListModel dfm = new DefaultListModel();
            Connection c = Conexao.ObterConexao();
            String SQL = "SELECT * FROM sc_aeroporto.avioes";
            PreparedStatement ps = c.prepareStatement(SQL);
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while(rs.next()){
                dfm.addElement(rs.getString("numeroIdentificacao"));
                listAviao.setModel(dfm);
                c.close();
            }
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarAeroporto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
    
     public void listar3(JComboBox listPiloto){
        
        try {
            listPiloto.removeAll();
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = Conexao.ObterConexao();
            PreparedStatement p = c.prepareStatement("SELECT * FROM sc_aeroporto.Piloto");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
                
                NumeroIdentificacao n = new NumeroIdentificacao(rs.getString("numeroidentificacao"));
                m.addElement(n);
                
                
            }
            listPiloto.setModel(m);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ListarAeroporto.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
    }
        
}
