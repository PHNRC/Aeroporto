package aeroporto;




import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Insercao {
    
    public void inserir(String rua, String numero, String pais, String cidade, String telefone, String bairro, String numeroFuncionarios, String nomeAeroporto, String codigo){
        try {
            Connection c = Conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_aeroporto.Aeroporto(rua,numero,pais,cidade,telefone,bairro,numeroFuncionarios,nomeAeroporto,codigo)values(?,?,?,?,?,?,?,?,?)");
            ps.setString(1,rua);
            ps.setInt(2,Integer.valueOf(numero));
            ps.setString(3,pais);
            ps.setString(4,cidade);
            ps.setInt(5,Integer.valueOf(telefone));
            ps.setString(6,bairro);
            ps.setInt(7,Integer.valueOf(numeroFuncionarios));
            ps.setString(8,nomeAeroporto);
            ps.setInt(9,Integer.valueOf(codigo));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirAviao(String nomeAeroporto, String modelo, String marca, String tamanho, String cor, String capacidade, String velocidadeMax, String numeroIdentificacao){
        
        try {
            Connection c = Conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_aeroporto.avioes(nomeAeroporto,modelo,marca,tamanho,cor,capacidade,velocidadeMax,numeroIdentificacao)values(?,?,?,?,?,?,?,?)");
            ps.setString(1,nomeAeroporto);
            ps.setString(2,modelo);
            ps.setString(3,marca);
            ps.setInt(4,Integer.valueOf(tamanho));
            ps.setString(5,cor);
            ps.setInt(6,Integer.valueOf(capacidade));
            ps.setInt(7,Integer.valueOf(velocidadeMax));
            ps.setInt(8,Integer.valueOf(numeroIdentificacao));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void inserirPiloto(String categoria, String nome, String numeroidentificacao, String cpf, String originalidade, String idade){
        
        try {
            Connection c = Conexao.ObterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_aeroporto.Piloto(categoria,nome,numeroidentificacao,cpf,originalidade,idade)values(?,?,?,?,?,?)");
            ps.setString(1,categoria);
            ps.setString(2,nome);
            ps.setInt(3,Integer.valueOf(numeroidentificacao));
            ps.setInt(4,Integer.valueOf(cpf));
            ps.setString(5,originalidade);
            ps.setInt(6,Integer.valueOf(idade));
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
