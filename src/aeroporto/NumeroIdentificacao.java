
package aeroporto;


public class NumeroIdentificacao {
    private int tamanho;
    private String cor;
    private String marca;
    
    private String categoria;
    private String  nome;
    private String numeroIdentificacao;
    private String cpf;
    private String originalidade;
    private String idade;

    public NumeroIdentificacao(int tamanho, String cor, String marca, String categoria, String nome, String cpf, String originalidade, String idade) {
        this.tamanho = tamanho;
        this.cor = cor;
        this.marca = marca;
        this.categoria = categoria;
        this.nome = nome;
        this.cpf = cpf;
        this.originalidade = originalidade;
        this.idade = idade;
    }

    public String getNumeroIdentificacao() {
        return numeroIdentificacao;
    }

    public void setNumeroIdentificacao(String numeroIdentificacao) {
        this.numeroIdentificacao = numeroIdentificacao;
    }
    
    

    public NumeroIdentificacao(String cor) {
        this.cor = cor;
    }

    public int getTamanho() {
        return tamanho;
    }

    public void setTamanho(int tamanho) {
        this.tamanho = tamanho;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getOriginalidade() {
        return originalidade;
    }

    public void setOriginalidade(String originalidade) {
        this.originalidade = originalidade;
    }

    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }

    @Override
    public String toString() {
        return  cor ;
    }

   
}
